var fs = require("fs");
var gui = require('nw.gui');

gui.Screen.Init();

// setup menu
(function(){
	// Create menu
	var menu = new gui.Menu({
	    type:   'menubar'
	});

	// Needs Mac Built In?
	menu.createMacBuiltin('lrbe');

	// var file_menu = new gui.Menu();

	// file_menu.append(new gui.MenuItem({label:"Open"}));

	// // Append MenuItem as a Submenu
	// menu.insert(
	//     new gui.MenuItem({
	//         label: 'File',
	//         submenu: file_menu
	//     }), 1
	// );

	// Append Menu to Window
	gui.Window.get().menu = menu;
})();

//var BASE_PATH = "/Users/slynn1324/Library/Application Support/Adobe/Lightroom/Layout Templates/";
var BASE_PATH = process.env.HOME + "/Library/Application Support/Adobe/Lightroom/Layout Templates/";






var app = angular.module('lrbe', ['ngRoute']);

app.config(['$routeProvider', '$sceProvider', 
	function($routeProvider, $sceProvider){
		$routeProvider.
			when("/", {
				templateUrl: 'home.html',
				controller: 'HomeCtrl'
			}).
			when("/pages/:book_size", {
				templateUrl: 'pages.html',
				controller: 'PagesCtrl'
			}).
			otherwise({
				redirectTo: '/'
			});

		$sceProvider.enabled(false);
	}]);


app.controller('HomeCtrl', ['$scope', 
	function($scope){
		$scope.book_sizes = BOOK_SIZES;

	}
]);

app.controller('PagesCtrl', ['$scope', '$routeParams', 
	function($scope, $routeParams){



		window.edit_done = function(svg, page){
			console.log(svg);
			console.log(page);
			update_page_from_svg(svg, page, $scope.book_size);
			console.log(page);
			//$scope.pages.push(page);
			$scope.$apply();
			console.log($scope.pages);
			//console.log(updated_page);
		};
		
		// read and parse the right templatePages.lua file
		console.log($routeParams);
		$scope.book_size = $routeParams.book_size;
	
		for ( var i = 0; i < BOOK_SIZES.length; ++i ){
			var book_size = BOOK_SIZES[i];
			if ( book_size.folder_name == $scope.book_size ){
				$scope.book_size_name = book_size.name;
			}
		}


		var lua = fs.readFileSync(BASE_PATH + $scope.book_size + "/custompages" + $scope.book_size + "/templatePages.lua", {encoding:"utf8"});


		// fixme, combine the to_json and parse_template_pages functions....
		var lua_json = LUA.to_json(lua, function(templatePages){

			$scope.pages = parse_template_pages(templatePages);

			$scope.thumbs = [];

			for ( var i = 0; i < $scope.pages.length; ++i ){
				var page = $scope.pages[i];

				page.svg = create_svg(page);
				page.svg_thumb = create_svg_thumb(page, $scope.book_size);

				//$scope.thumbs.push({pageId:page.pageId, svg: create_svg(page), thumb_svg: create_svg_thumb(page)});
			}

		});

		// add the 'default pages'
		var default_pages = APP_CONFIG.default_pages[$scope.book_size];
		$scope.default_pages = [];
		for( var i = 0; i < default_pages.length; ++i ){
			var dp = default_pages[i];
			dp.is_default = true;
			dp.areas = [];
			dp.svg = create_svg(dp);
			dp.svg_thumb = create_svg_thumb(dp, $scope.book_size);
			$scope.default_pages.push(dp);
		}


		// setup the 'export' function
		$scope.export = function(){

			var pages = $scope.pages;


			var export_pages = [];
			for ( var i = 0; i < pages.length; ++i ){
				var page = pages[i];
				if ( ! page.is_default ){
					export_pages.push(page);
				}
			}

			var lua = create_lua(export_pages);
			console.log(lua);

			var jpgs = [];
			for ( var i = 0; i < export_pages.length; ++i ){
				var page = export_pages[i];

				var jpg = create_jpg_thumb(page, $scope.book_size);

				jpgs.push({pageId:page.pageId, jpg: jpg});

			}

			console.log(jpgs);

			// create a backup copy of the template dir
			var dir = BASE_PATH + $scope.book_size + "/custompages" + $scope.book_size;

			var home_dir = process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];

			var backup_dir = home_dir + "/" + "lightroom_book_template_backups/";
			fs.mkdirSafeSync(backup_dir);
			
			if ( ! fs.statSync(backup_dir).isDirectory() ){

				try{
					fs.mkdirSync(backup_dir);
				} catch (e){
					alert("Error creating backup dir: " + backup_dir);
					throw e;
				}
			}

			var book_size_backup_dir = backup_dir + $scope.book_size;

			fs.mkdirSafeSync(book_size_backup_dir);

			if ( ! fs.statSync(book_size_backup_dir) ){
				try{
					fs.mkdirSync(book_size_backup_dir);
				} catch (e){
					alert("Error creating dir for book size: " + book_size_backup_dir);
					throw e;
				}
			}
				

			fs.renameSync(dir, backup_dir + $scope.book_size + "/custompages" + $scope.book_size + "-" + new Date().getTime())

			fs.mkdirSync(dir);

			fs.writeFileSync(dir + "/templatePages.lua", lua, {encoding:"utf8"});
			for(var i = 0; i < jpgs.length; ++i ){
				var jpg = jpgs[i];
				fs.writeFileSync(dir + "/" + jpg.pageId + ".jpg", jpg.jpg, 'base64');
			}
			
			
		};

		$scope.edit = function(page){

			var svg = page.svg;

			var bg_url = $scope.book_size;

			if ( page.isSpread ){
				bg_url += '-spread';
			}

			bg_url += '-bg.jpg';

			var svg_src = '?baseUnit=in&bkgd_url=' + encodeURIComponent('../' + bg_url) + '&source=' + encodeURIComponent('data:image/svg+xml;base64,' + btoa(svg));

			console.log(svg_src);

			var edit_wnd = gui.Window.open('file://' + process.cwd() + '/svg-edit-2.6/svg-editor.html' + svg_src, {width: 1024, height: 768, focus:true, toolbar: false});

			edit_wnd.on('loaded', function(){
			   edit_wnd.window.setParent(gui.Window.get(), page);
			   //console.log(edit_wnd);
			});

			//window.open('svg-edit-2.6/svg-editor.html' + svg_src);

		};

		$scope.duplicate = function(page){
			var clone = {};

			clone.pageId = uuid();
			clone.height = page.height;
			clone.width = page.width;
			clone.isSpread = page.isSpread;
			clone.areas = [];
			clone.svg = page.svg;
			clone.svg_thumb = page.svg_thumb;

			for( var i = 0; i < page.areas.length; ++i ){
				clone.areas.push(page.areas[i]);
			}

			$scope.pages.push(clone);

			return clone;
		}

		$scope.add = function(template_page){
			var page = $scope.duplicate(template_page);
			page.pageId = uuid();
			$scope.edit(page); 
		}

		$scope.delete = function(page){
			if ( window.confirm("Are you sure you want to delete this template?") ){
				var index = $scope.pages.indexOf(page);
				$scope.pages.splice(index,1);
			}
		}

	}
]);

app.filter('default_page_class', function(){
	return function(input){
		if( input ){
			return "default";
		} else {
			return "";
		}
	}
});


fs.mkdirSafeSync = function(path){
	try{
		fs.mkdirSync(path);
	} catch(e) {
		if ( e.code != 'EEXIST' ) throw e;
	}
}

Array.prototype.insert = function (index, item) {
  this.splice(index, 0, item);
};




