var fs = require("fs");


var BOOK_SIZES = [
	{name:"Small Square (7x7 in)", folder_name:"7x7-blurb", thumb_w:100, thumb_h:100, spread_thumb_w:185, spread_thumb_h:92},
	{name:"Standard Landscape (10x8 in)", folder_name:"10x8-blurb", thumb_w:116, thumb_h:100, spread_thumb_w:186, spread_thumb_h:79},
	{name:"Standard Portrait (8x10 in)", folder_name:"8x10-blurb", thumb_w:76, thumb_h:100, spread_thumb_w:157, spread_thumb_h:100},
	{name:"Large Landscape (13x11 in)", folder_name:"13x11-blurb", thumb_w:116, thumb_h:100, spread_thumb_w:185, spread_thumb_h:79},
	{name:"Large Square (12x12 in)", folder_name:"12x12-blurb", thumb_w:98, thumb_h:100, spread_thumb_w:185, spread_thumb_h:93}
];

var BOOK_SIZES_MAP = {};
for ( var i = 0; i < BOOK_SIZES.length; ++i ){
	var b = BOOK_SIZES[i];
	BOOK_SIZES_MAP[b.folder_name] = b;
}

function get_book_size(id){
	return BOOK_SIZES_MAP[id];
}

var HBAR = {
	svg : Handlebars.compile(fs.readFileSync("handlebars_templates/svg.hbar", {encoding:"utf8"})),
	templatePages_10x8 : Handlebars.compile(fs.readFileSync("handlebars_templates/templatePages_10x8.hbar", {encoding:"utf8"})),
	thumb_div : Handlebars.compile(fs.readFileSync("handlebars_templates/thumb_div.hbar", {encoding:"utf8"}))
};

function parse_template_pages(lua_obj){
	var _pages = [];
	for ( var i = 0; i < lua_obj.pages.length; ++i ){
		var lua_page = lua_obj.pages[i];
		var page = {
			"pageId":lua_page.pageId, 
			"width":lua_page.pageWidth, 
			"height":lua_page.pageHeight, 
			"isSpread":lua_page.isSpread === true,
			"areas":[]
		};
		var lua_areas = lua_page[1].children;
		for ( var j = 0; j < lua_areas.length; ++j ){
			var lua_area = lua_areas[j].transform;
			page.areas.push({"x":lua_area.x,"y":lua_area.y,"width":lua_area.width,"height":lua_area.height});
		}

		page.areas.reverse()

		_pages.push(page);
	}
	return _pages;
}

function create_thumbs(pages){
	var results = [];

	debugger;

	for ( var i = 0; i < pages.length; ++i ){
		var page = pages[i];

		console.log("page");
		console.log(page);

		var svg = create_svg_thumb(page);

		results.push(svg);
	}

	return results;
}

function create_svg_thumb(page, book_size){
	// clone the page so we can't modify it...
	var page = JSON.parse(JSON.stringify(page));

	var scale = 1;

	// find the matching book_size
	var book_size = BOOK_SIZES_MAP[book_size];


	if ( page.isSpread ){
		var h_scale = book_size.spread_thumb_h / page.height;
		var w_scale = book_size.spread_thumb_w / page.width;

		scale = w_scale;
		//if ( w_scale < scale ){
		//	scale = w_scale;
		//}
	} else {
		var h_scale = book_size.thumb_h / page.height;
		var w_scale = book_size.thumb_w / page.width;

		scale = w_scale;
		//if ( w_scale < scale ){
		//	scale = w_scale;
		//}
	}

	console.log(scale);

	for ( var i = 0; i < page.areas.length; ++i ){
		var area = page.areas[i];
		// rect.y = view.pageHeight-(rect.y+rect.height);
		area.y = page.height - ( area.y + area.height );

		area.y = area.y * scale;
		area.x = area.x * scale;
		area.height = area.height * scale;
		area.width = area.width * scale; 
	}

	page.height = page.height * scale;
	page.width = page.width * scale;

	var svg = HBAR.svg(page);

	return svg;
}

function create_svg(page){
	// clone the page so we can't modify it...
	var page = JSON.parse(JSON.stringify(page));

	for ( var i = 0; i < page.areas.length; ++i ){
		var area = page.areas[i];

		area.y = page.height - (area.y + area.height);
	}

	var svg = HBAR.svg(page);

	return svg;
}

function create_jpg_thumb(page, book_size){
	console.log("creating jpg for " + page.pageId + "book_size=" + book_size);

	var width;
	var height;
	if ( page.isSpread ){
		width = BOOK_SIZES_MAP[book_size].spread_thumb_w;
		height = BOOK_SIZES_MAP[book_size].spread_thumb_h;
	} else {
		width = BOOK_SIZES_MAP[book_size].thumb_w;
		height = BOOK_SIZES_MAP[book_size].thumb_h;
	}

	//var canvas = $("#canvas")[0];

	var div = document.createElement('div');
	div.innerHTML = '<canvas height="' + height + 'px" width="' + width + 'px"></canvas>';
	var canvas = div.firstChild;

	var svg = create_svg_thumb(page, book_size);

	var ctx = canvas.getContext('2d');
	ctx.fillStyle = 'white';
	ctx.fillRect(0,0,width,height);
	ctx.drawSvg(svg);//, 0, 0, 186, 179);

	var jpg = canvas.toDataURL("image/jpeg");

	var base64 = jpg.substring(23);

	console.log(base64);

	return base64;

}

function create_lua(pages){
	console.log("creating lua for pages:");
	console.log(pages);
	var pages_clone = JSON.parse(JSON.stringify(pages));
	for ( var i = 0; i < pages_clone.length; ++i ){
		pages_clone[i].areas.reverse();
	}
	return HBAR.templatePages_10x8({"pages":pages_clone});
}

function update_page_from_svg(svg, page, book_size){

	console.log("parsing svg=");
	console.log(svg);

	var svg_doc = $.parseXML(svg);

	var $svg = $(svg_doc).find("svg");

	var areas = [];

	$svg.find("rect").each(function(){
		var area = {"x":parseFloat($(this).attr("x")),"y":parseFloat($(this).attr("y")),"height":parseFloat($(this).attr("height")),"width":parseFloat($(this).attr("width"))};
		area.y = page.height - (area.y + area.height);
		areas.push(area);
	});

	page.areas = areas;

	console.log(page);

	page.svg = create_svg(page);
	page.svg_thumb = create_svg_thumb(page, book_size);

}

function uuid(){
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
	    return v.toString(16);
	});
}
