var APP_CONFIG = {
	default_pages: {
		"10x8-blurb" : [			
			{width: 693, height: 594},
			{width: 1386, height: 594, isSpread: true}
		],
		"8x10-blurb" : [
			{width: 567, height: 720},
			{width: 1134, height: 720, isSpread: true}
		],
		"7x7-blurb" : [
			{width: 495, height: 495},
			{width: 990, height: 495, isSpread: true}
		],
		"12x12-blurb" : [
			{width: 855, height: 864},
			{width: 1710, height: 864, isSpread: true}
		],
		"13x11-blurb" : [
			{width: 909, height: 783},
			{width: 1818, height: 783, isSpread: true}
		]

	}
};