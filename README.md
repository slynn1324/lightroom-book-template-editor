# Lightroom Book Template Editor (LRBE)#

Lightroom's Book module allows for very easy creation of great looking books -- however it does not allow you to create custom templates in any way.  With some looking into the saved template format, I've started an editor to simplify the creation of custom book layout templates.  

This project is implemented using node-webkit (https://github.com/rogerwang/node-webkit).

The drawing is handled through an SVG editor (http://svg-edit.googlecode.com). The editor is used to draw the rectangles, and then the svg xml is parsed to be turned into the lua template file format. 

Not pretty, but allows creating and editing lightroom book layout templates, which is not possible within lightroom itself by using an SVG editor.  

Currently only tested on Mac OS.  If somebody would like to contribute the configuration and testing for Windows, it would be appreciated.  I do not currently have access to Lightroom on a Windows machine to determine the proper file paths and do testing.  Because node-webkit is cross platform, it should be relatively simple to get it working. 

Before using LRBE, it is recommended to create the expected custom templates directories in Lightroom by going through each book size, and saving a custom template by right-clicking on a page and selecting "Save As Custom Page".  Once each page size has it's template directories established, you can launch LRBE and should be able to create and modify templates.

To save the templates out of LRBE, choose save and the Lua file and corresponding jpg thumbnails will be created.  These files will overwrite the existing lightroom files for the chosen book size, and the existing book files will be backed up to a .lightroom_book_template_backups directory in your home directory.

While this program has worked for me on my machine, I can't guarantee that you'll have the same results.  Please make sure you have backups of anything important before using this program as this is very "Alpha" quality software at this point. 
